Drupal out of the box supports blog functionality. But like other blogging
platforms, it does not provide a archive, based on creation time.
This can be easily done using views to have a monthly archive, but creating a
monthly archive group by year or creating a archive
which shows title (as a link to the content) group by month and group by year is
 not possible.
This module creates a block for blog archive. Options are there to configure
whether to have a monthly archive or monthly archive group by year
or blog title archive group by month and group by year.

Dependencies:
External Links - http://drupal.org/project/views
Drupal blog module
