<?php

/**
 * @file
 * Module file for blog_archive_by_creation_time.
 */

/**
 * Implements hook_menu().
 */
function blog_archive_by_creation_time_menu() {
  $items = array();
  $items['admin/config/blog-archive'] = array(
    'title' => 'Blog Archive',
    'position' => 'left',
    'weight' => -100,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/blog-archive/settings'] = array(
    'title' => 'Blog Archive Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('blog_archive_by_creation_time_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Setting for hierarchy for displaying blogs.
 */
function blog_archive_by_creation_time_admin_settings($form, &$form_state) {
  $form['blog_archive_by_creation_time_level'] = array(
    '#title' => t('Select hierarchy level for blog archive'),
    '#type' => 'radios',
    '#options' => array(
      'm' => t('Months'),
      'ym' => t('Months group by year'),
      'mt' => t('Blog title group by months'),
      'ymt' => t('Blog title group by months group by year'),
    ),
    '#default_value' => variable_get('blog_archive_by_creation_time_level', 'ymt'),
  );
  $form['blog_archive_by_creation_time_length'] = array(
    '#title' => t('Maximum length of the blog title'),
    '#type' => 'textfield',
    '#default_value' => variable_get('blog_archive_by_creation_time_length', 72),
    '#states' => array(
      // Hide the settings when the cancel notify checkbox is disabled.
      'visible' => array(
        ':input[name="blog_archive_by_creation_time_level"]' => array(array('value' => 'ymt'), array('value' => 'mt')),
      ),
    ),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_block_info().
 */
function blog_archive_by_creation_time_block_info() {
  $blocks['blog-archive'] = array(
    'info' => t('Blog Archive'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function blog_archive_by_creation_time_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'blog-archive':
      $block['subject'] = ('Blog Archive');
      $block['content'] = blog_archive_by_creation_time_create_archive();
      break;
  }
  return $block;
}

/**
 * Callback for blog-archive block contents.
 */
function blog_archive_by_creation_time_create_archive() {
  drupal_add_js('misc/form.js', 'file');
  drupal_add_js('misc/collapse.js', 'file');
  $output = array();
  // Find the hierarchy level admin has selected from configuration page.
  $hierarchy_level = variable_get('blog_archive_by_creation_time_level', "ymt");
  if ($hierarchy_level == "ymt") {
    $result = blog_archive_by_creation_time_query_blog_node();
    foreach ($result as $record) {
      $year = date('Y', $record->created);
      $month = date('F', $record->created);
      if (!isset($output[$year])) {
        $output[$year] = array(
          '#type' => 'fieldset',
          '#title' => t($year),
          '#attributes' => array('class' => array('collapsible', 'collapsed')),
        );
      }
      if (!isset($output[$year][$month])) {
        $output[$year][$month] = array(
          '#type' => 'fieldset',
          '#title' => t($month),
          '#attributes' => array('class' => array('collapsible', 'collapsed')),
        );
        $output[$year][$month]['blog-link'] = array(
          '#items' => array(),
          '#theme' => 'item_list',
        );
      }
      $blog_title = truncate_utf8($record->title, variable_get('blog_archive_by_creation_time_length', 72), TRUE, TRUE);
      $output[$year][$month]['blog-link']['#items'][] = l(t($blog_title), 'node/' . $record->nid);
    }
  }
  elseif ($hierarchy_level == "ym") {
    $result = blog_archive_by_creation_time_query_blog_node_groupby_month_year();
    foreach ($result as $record) {
      $year = $record->year;
      $month_num = $record->monthnum;
      if (!isset($output[$year])) {
        $output[$year] = array(
          '#type' => 'fieldset',
          '#title' => t($year),
          '#attributes' => array('class' => array('collapsible', 'collapsed')),
        );
        $output[$year]['month-link'] = array(
          '#items' => array(),
          '#theme' => 'item_list',
        );
      }
      $link_text = $record->month . ' (' . $record->count . ')';
      $output[$year]['month-link']['#items'][] = l(t($link_text), 'archive/blog/' . $year . sprintf("%02s", $month_num));
    }
  }
  elseif ($hierarchy_level == "m") {
    $result = blog_archive_by_creation_time_query_blog_node_groupby_month_year();
    foreach ($result as $record) {
      $month_num = $record->monthnum;
      if (!isset($output['month-link'])) {
        $output['month-link'] = array(
          '#items' => array(),
          '#theme' => 'item_list',
        );
      }
      $link_text = $record->month . '-' . $record->year . ' (' . $record->count . ')';
      $output['month-link']['#items'][] = l(t($link_text), 'archive/blog/' . $year . sprintf("%02s", $month_num));
    }
  }
  else {
    $result = blog_archive_by_creation_time_query_blog_node();
    foreach ($result as $record) {
      $year = date('Y', $record->created);
      $month = date('F', $record->created);
      $month = $month . '-' . $year;
      if (!isset($output[$month])) {
        $output[$month] = array(
          '#type' => 'fieldset',
          '#title' => t($month),
          '#attributes' => array('class' => array('collapsible', 'collapsed')),
        );
        $output[$month]['blog-link'] = array(
          '#items' => array(),
          '#theme' => 'item_list',
        );
      }
      $blog_title = truncate_utf8($record->title, variable_get('blog_archive_by_creation_time_length', 72), TRUE, TRUE);
      $output[$month]['blog-link']['#items'][] = l(t($blog_title), 'node/' . $record->nid);
    }
  }
  return $output;
}

/**
 * Get blog node count, group by month and year.
 */
function blog_archive_by_creation_time_query_blog_node_groupby_month_year() {
  $result = db_query('SELECT MONTH(FROM_UNIXTIME(created)) AS monthnum,
              MONTHNAME(FROM_UNIXTIME(created)) AS month,YEAR(FROM_UNIXTIME(created)) AS year ,
              count(*) as count FROM {node} n WHERE n.type = :type and n.status = 1
              GROUP BY CONCAT(month,year) ORDER BY n.created DESC', array('type' => 'blog'));
  return $result;
}

/**
 * Get all blog node.
 */
function blog_archive_by_creation_time_query_blog_node() {
  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'title', 'created'))
    ->condition('n.type', 'blog', '=')
    ->condition('n.status', 1)
    ->orderBy('n.created', 'DESC');
  $result = $query->execute();
  return $result;
}

/**
 * Implements hook_views_api().
 */
function blog_archive_by_creation_time_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'blog_archive_by_creation_time'),
  );
}
